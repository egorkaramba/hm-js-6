// 1) 
const product = {
    name: 'eggs',
    price: 50,
    discount: 10
};
product.discountPrice = function () {
    return this.price * (1 - this.discount / 100)
};

console.log(product.discountPrice());

// 2 ) 
function createGreeting(obj) {
    return "Hello, i`m " + obj.name + ",i`m " + obj.age + " years old";
}

const userName = prompt("Введіть ваше ім'я:");
const userAge = prompt("Введіть ваш вік:");

const userObject = {
    name: userName,
    age: userAge
};

alert(createGreeting(userObject));
